//Yamelin Jaquez
//CSE02
//December 4, 2018
//using 2D and 3D arrayays to let user play a game of tic tac toe

import java.util.*;
import java.util.arrays;

public class HW10{
    public static void main(String [] args){
   
        char[] [] numSequence = {{'1','2','3'},{'4','5','6'}, {'7','8','9'}};
        printBoard(numSequence); //printing out the board of numbers, taken from the method below
        int counter = 2; //when is even is player 1, when is odd is player 2
        Scanner myScanner = new Scanner (System.in);

        while (checkForWinorEndofGame(numSequence) == false){ //while loop while the number sequence array is being checked
            //for errors and if the user wins or loses the game
           
            System.out.println("Please enter the coordinates 0-2");
            int inputRow; //integer will stand for what the user enters as rows
            if (myScanner.hasNextInt()){  //if statement will check that the input is an integer
               inputRow = myScanner.nextInt(); 
            } else { 
                System.out.println("PLease enter the coordinates 0 -2 "); 
                continue;//will go back to the if to check the condition
            } 
            if ( inputRow < 0 || inputRow > 2){ //if statement that will make sure the coordinate for row entered are from 0-2
                System.out.println("Error: please enter a number from 0-2");
                continue; 
            }

            int inputColumn; //integer that will stand for the user input for column
            if (myScanner.hasNextInt()){ //if the input is an integer
               inputColumn = myScanner.nextInt(); //then whatever the user inputted, will be correct
            } else {
                System.out.println("Please enter the coordinates 0-2"); //if not, error and prompt to enter again
                continue;
            } 
            if ( inputColumn < 0 || inputColumn > 2){ //to check that number for column coordinate is between 0 and 2
                System.out.println("Error: please enter a number from 0-2");
                continue;
            }
            if ( numSequence[inputRow][inputColumn] == 'X'  || numSequence[inputRow][inputColumn] == 'O'){ //if statement if the coordinate falls in a space in the array thats being used
                System.out.println("Cannot used this spot");
                
            }

            if ( counter % 2  == 0 ){ //this if statement will place the X or 0 where the user prompts it tp
                numSequence[inputRow][inputColumn] = 'O';
            } else {
                numSequence[inputRow][inputColumn] = 'X';
               
            }  
            printBoard(numSequence); //calling the method that will prompt the board
            counter ++;

          
        }

        if (isTie (numSequence) == true){ //if the method that checks the tie is true, the spot can no longer be used
            System.out.println("Cannot used this spot");
        }
        else if ( counter % 2  == 0 ){ 
            System.out.println("Player 1 won"); 
        } else {
            System.out.println("Player 2 won");
        }


    }

        public static void printBoard (char [][] numSequence){ //method for the board
    
                for (int row = 0; row <3; row++){ //for loop to print out the numbers 1-9 as a board

                for (int column = 0; column < 3; column++){
                System.out.print(numSequence[row][column] + " "); 
                }
                System.out.println();
        }
    }
        public static boolean  isTie(char [][] numSequence ){ //method that checks if the game ends in a tie


         for (int i = 0; i < 3; i++ ){
            for (int j= 0; j < 3; j++){
                if (numSequence[i][j] != 'X' || numSequence[i][j] !='O' ){ // checks if the spaces in the array are filled
                    return false;
                }
            }
        }

        return true;
        }

        public static boolean checkForWinorEndofGame(char[][] array){ //method that checks who wins, or if the game ends
 
            isTie (array); //calling the tie method
            if (isTie(array) == true){
                return true;
            }
                //checking the coordinate in the board 
           if  ((array[0][0] == array[0][1] && array[0][0] == array[0][2])
            || (array[0][0] == array[1][1] && array[0][0] == array[2][2])
            || (array[0][0] == array[1][0] && array[0][0] == array[2][0])
            || (array[2][0] == array[2][1] && array[2][0] == array[2][2])
            || (array[2][0] == array[1][1] && array[0][0] == array[0][2])
            || (array[0][2] == array[1][2] && array[0][2] == array[2][2])
            || (array[0][1] == array[1][1] && array[0][1] == array[2][1])
            || (array[1][0] == array[1][1] && array[1][0] == array[1][2]))
            return true ;
            else {
                return false;
            }
        }//end of main method


} //end of class

