//CSE02- Hw09
//November 14, 2018
//Using methods and arrays to print out a shuffled deck of cards

import java.util.Arrays; //importing the Array class
import java.util.*; //importing all classes

public class Shuffling {
    public static void main(String[] args) { //main method
        Scanner scan = new Scanner(System.in);
        String[] hand = new String[5]; //hand determines how many times, cards are given to the user
        int numCards = 5; //the amount of cards the user gets at each hand
        int again = 1; //delcaring integer for the loop
        int index = 51; //where the card number ends
        String[] cards = newDeck(); //array of cards is equal  the new deck method
        System.out.println("");
        printArray(cards); //print arrray method. The paramete lies with the amount of cards
        System.out.println("Shuffled"); //printing out shuffled after cards have been shuffled
        shuffle(cards); //shuffle method with parameeter of cards
        printArray(cards); //using print method again to show array
        System.out.println("Hands"); //printing out hands 
        while (again == 1) { //start of while loop to generate another hand of cards with differnt cards
            hand = getHand(cards, index, numCards); //calling the get hand method to generate hand
            printArray(hand);
            index = index - numCards; //index is tne last element, here I am subtracting the index minus the number of cards
            System.out.println("Enter a 1 if you want another hand drawn");
            if (numCards > index) { //if the number in the cards is bigger than the index (51)
                cards = newDeck(); //calling the new deck method that will generate differnt cards
                shuffle(cards);
                index = 51;
                System.out.println("The cards were reseted. "); //output shows that the cards were reseted, ans new ones come in
            }
            again = scan.nextInt(); //lets the user input again, if the number of cards if bigger than the index
        }
    } //end of main method
    public static void printArray(String[] list) { //method to print an array
        System.out.print(Arrays.toString(list) + " "); //to string just 
        System.out.println("");
    }

    public static void shuffle(String[] cards) { //start of shuffle method

        for (int i = 0; i < 50; i++) { //start of for loop to shuffle 50 times
            String temp = ""; //string for temp value
            int randomindex = (int)(Math.random() * 51 + 1); //randomindex is the integer that will stand for the random index
            temp = cards[0]; //temp is a string that will hold the card element 0, the start index
            cards[0] = cards[randomindex]; // the start element is equal to the random card value
            cards[randomindex] = temp; //finally, the random card will equal the temporate value
        }
    }

    public static String[] getHand (String[] cards, int index, int numCards) { //start of method for get hand
        String[] newArray = new String[numCards]; //this is a new array
        for (int i = 0; i + index - numCards < index; i++) { //start of for loop that will increments i
            //this for loop means that i added to the index minus the number of cards (5) is less than index
            newArray[i] = cards[index - i]; //the new array will hold this increment as an element
            //this array will equal (index - i) as the new element
        }
        return newArray;
    }
   
    public static String[] newDeck() { //method that will generate a new deck if the index is bigger thant cardd hand
        String[] suitNames = { "C", "H", "S","D"};
        String[] rankNames = {  "2",  "3", "4", "5","6", "7", "8", "9","10", "J","Q","K","A"};
        String[] cards = new String[52];
        for (int i = 0; i < 52; i++) {
            cards[i] = rankNames[i % 13] + suitNames[i / 13];
            System.out.print(cards[i] + "                          ");
        }
        return cards;
    }
} //endofclass