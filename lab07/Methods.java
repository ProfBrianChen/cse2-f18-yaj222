//Yamelin Jaquez
//CSE02 Lab07
//Using methods to generate random senteces, and a paragraph with random parts to it. 

import java.util.*; //importing all classes for java; includes all types 

public class Methods {
    public static void main(String[] args) { //start of main method

        Scanner myScanner = new Scanner(System.in); //declaring system for user input 
        System.out.println("Enter 1 for new sentence, enter 2 to exit"); //prints our line to ask user for input
        int newSentence = myScanner.nextInt(); //declaring user input to variable

        //start of while loop that genrates what the user inputs
        while (newSentence != 2) { //if the user picks 1
            String retVal = thesisSentence(); //declaring an integer to call the thesis sentence method
            System.out.println("Enter 1 for new sentence, enter 2 to exit"); 
            newSentence = myScanner.nextInt(); //declaring user input in variable
        } 
        System.out.println();
        System.out.println();

        createParagraph(); //calling the method for crating a paragraph
        System.out.println();


    }
    public static String thesisSentence() { //start of method to create the thesis sentence 
        String Subject = nonPrimNounSub(); //declaring subject, and calling the method to generate subjects
        System.out.println("The" + " " + Adjectives() + " " + Adjectives() + " " + Subject + " " + pastVerbs() + " the" + " " + Adjectives() + " " + nonPrimNounSub() + ".");
        return Subject; //returning the subject to be later used in the next sentences

    }
    public static void actionSentence(String Subject) { //start of action method to make an action sentence, subect in the parameters from thesis method
        int randomInt = (int)(Math.random() * 2) + 3; //generating random sentences with numbers 0-2, +3 just makes 3 more sentences
        if (randomInt == 1) { //start of if stamement that declares if the random number is equal to 1 
            System.out.print("It" + " " + "used " + nonPrimNounsObj() + " " + "to " + pastVerbs() + " " + nonPrimNounsObj() + " at the " + Adjectives() + " " + nonPrimNounSub() + ".");
        } else { //else for the other numbers generate other than 1 
            System.out.print("This " + Subject + " was " + Adjectives() + " " + Adjectives() + " to " + pastVerbs() + " " + nonPrimNounsObj() + ".");
        }
    }
    public static void createParagraph() { //start of method to create a paragraph
        String Subject = thesisSentence(); //declarring the subject string to equal the thesis stamement method call
        int randomInt = (int)(Math.random() * 5); //random numbers from 0-4, that will generate the sentences
        for (int i = 0; i < randomInt; i++) { //start of for loop to use the same subject from thesis statement inside the action, and conclusion
            actionSentence(Subject);
        }
        conclusionSentence(Subject);

    }

    public static void conclusionSentence(String Subject) { //method to generate a conclusion sentence
        System.out.print("That " + Subject + " " + pastVerbs() + " her " + nonPrimNounsObj() + ".");


    }

    public static String Adjectives() { //method to generate different types of adjectives
        int randomInt = (int)(Math.random() * 10); //decalring a random integer that will generate from 0-9 and will pick one for the sentences in the other methods
        switch (randomInt) { //switch statement will provide the differnt cases 
            case 1:
                return "beautiful";
            case 2:
                return "clean";
            case 3:
                return "orange";
            case 4:
                return "itchy";
            case 5:
                return "powerful";
            case 6:
                return "rich";
            case 7:
                return "lazy";
            case 8:
                return "skinny";
            case 9:
                return "wide";
            default:
                return "happy";

        } //end os switch

    } //end of Adjectives method


    public static String nonPrimNounSub() { //start of the noun Subject method
        int randomInt = (int)(Math.random() * 10); //declaring a random integer to generate random subjects

        switch (randomInt) { //switch statements to return the words for the random number selected
            case 1:
                return "student";
            case 2:
                return "teacher";
            case 3:
                return "worker";
            case 4:
                return "mother";
            case 5:
                return "father";
            case 6:
                return "president";
            case 7:
                return "member";
            case 8:
                return "grandfather";
            case 9:
                return "daughter";
            default:
                return "babies";
        }

    } //end of nonPrimNounSub method

    public static String pastVerbs() {
        int randomInt = (int)(Math.random() * 10) + 1;
        switch (randomInt) {  //switch statements to return the words for the random number selected
            case 1:
                return "student";
        
            case 2:
                return "talked";
            case 3:
                return "laughed";
            case 4:
                return "ran";
            case 5:
                return "fell";
            case 6:
                return "built";
            case 7:
                return "caught";
            case 8:
                return "chose";
            case 9:
                return "drank";
            default:
                return "laughed";
        }


    }
    public static String nonPrimNounsObj() {
        int randomInt = (int) Math.random() * 10;

        switch (randomInt) {  //switch statements to return the words for the random number selected
            case 1:
                return "student";
            case 2:
                return "monkey";
            case 3:
                return "football";
            case 4:
                return "space";
            case 5:
                return "yarn";
            case 6:
                return "water";
            case 7:
                return "computers";
            case 8:
                return "phones";
            case 9:
                return "house";
            default :
                return "love";
        } //
    
    }
}//end of class