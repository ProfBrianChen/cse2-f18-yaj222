//Yamelin Jaquez
//CSE02 Hw 09 part 1
//Practicing single dimensional arrays

import java.util.*;
import java.util.Arrays;
 
    public class CSE2Linear { 
        public static void main (String [] args){ //main method
        Scanner myScanner = new Scanner (System.in); 
       int userInput; //declaring the user input for the array
       int [] arrayInput = new int [15]; //declaring the array that will save 15 spots for the user
        System.out.println("Enter 15 final grades: "); //prompting the user to enter 15 gradse
        int i = 0; 
            for ( i = 0; i < 15 ; i++){ //start of for loop to print out the array
                if (!myScanner.hasNextInt()){ //if statement that states if the input is not an integer to generate again until the user does
                    System.out.println("Error. Enter an integer: "); 
                    continue;
                } else { //if not then, the user can input again
                    userInput = myScanner.nextInt(); 
                  }
                if (userInput < 1 || userInput > 100){ //if statement that makes sure the integers are from 1 to 100
                    System.out.println("Error. Enter the integers from 1 and 100 only: ");
                    continue;
                  }
                if (i > 0 && userInput <  arrayInput[i - 1]){ //if statement that makes sure that the integers are in order
                    System.out.println("Error. Enter 15 integers between 1 and 100 in order from least to greatest: ");
                    continue;
                  }
                arrayInput[i] = myScanner.nextInt(); //saving the array
                }   
                 
        
              for (int j = 0; j < arrayInput.length; j++){ //for loop that prints the array
              System.out.print(arrayInput[j] + " ");
   }
             System.out.println("Enter the grade to search: "); //prompt that will let the user search the grade
            int gradeInput = myScanner.nextInt(); 
            binarySearch(arrayInput, gradeInput); //calling the method to use binary search to search the grade int he array
            shuffle (arrayInput); //calling the method that shuffles the array 
            System.out.println("Enter the grade to be searched : "); //since it was suffled, and if the grade was not found, it prompts the user to enter the grade again
            userInput = myScanner.nextInt();
            linearSearch(arrayInput, userInput); //calling the method for linear search to search for the grade again

    }
        public static void binarySearch(int[] theInput, int num) { //method that uses binary search
       int start = 0; //this will be the inetger for the start of the array
       int end = theInput.length - 1; //this is the intger for the end
       int middle = (start + end) / 2; //this is the integer for the middle
       int count = 0; //integer to count
  
       while (start <= end) { //while looop that works if the start is less than or equal to the end
            count++; 
           if (theInput[middle] < num) { //if statement that works only if, the middle of the array is less than number
               start = middle + 1; //declaring the start as the middle number added by one
           } else if (theInput[middle] == num) { //else that number is equal to the number
               System.out.println( num + " was found with " + count + " iterations"); //printing out how many times the number was found
               return; //returns that number
           } else { //else, then the end is equal to the middle number subtracted by one
             end = middle - 1;
           }
           middle = (start + end) / 2; // continuing the while loop, the middle will be the sum of start and the end divided by 2
       }
       System.out.println(" this number"  + num + " was not found with " + count + " iterations"); //if none of the conditions were met, then this number was not found
       return;

   }

 public static void shuffle(int[] arrayInp ){ //new method for the shuffling of the numbers
 
 for (int i = 0; i < 50; i++){ //for loop that will count up to 50
   int temp;  //integer for temporary hold
   int randIndex = (int) (Math.random () * arrayInp.length); //math random will shuffle the numbers inside the array
   temp = arrayInp[0]; //the array input index at 0
   arrayInp[0] = arrayInp[randIndex]; //the array inpout at 0 is equal to the random index
   arrayInp[randIndex] = temp;  //now the temporary hold integer is equal to the array, and the random index
   
   }
 }
 public static void linearSearch(int[] array, int num){ //method for the linear search
   int i = 0;
   for(i = 0; i < array.length; i++){ //for loop that will count the array lenght
     if( array[i] == num){ //if statement that workd if the number inside the array is equal to the grade
       System.out.println(num + " was found with " + i + " itterations"); //outputting that the number was found and how many times
     return;
     }
     
   }
   System.out.println(num + " was not found with " + i + " itterations"); //if not, then the number was not found

  } 
}//end of class