//Yamelin Jaquez
//CSE02 hw09 part 2
/// hw 09
/// create code that practices arrays and single dimensional arrays  

import java.util.*;
import java.util.Arrays;

public class RemoveElements {

 public static void main(String[] args) { //main method

  Scanner myScanner = new Scanner(System.in);
  //using Carr's provided code for most of this assingment 

  int number[] = new int[10]; //declaring an array that holds 10 elements
  int array1[]; //declaring array 1
  int array2[]; //declaring array2
  int index, target; //declaring the index and targer as integers
  String answer = ""; //the answer is decalred as a string

  do { //start of do while loop 
   System.out.print("Random input 10 ints [0-9]"); //ouputting that the system will generate random input of 10 numbers
   number = randomInput();
   String out = "The original array is:";
   out += listArray(number);
   System.out.println(out);

   System.out.print("Enter the index ");
   index = myScanner.nextInt();
   array1 = delete(number, index);
   String out1 = "The output array is ";
   out1 += listArray(array1);
   System.out.println(out1);

   System.out.print("Enter the target value ");
   target = myScanner.nextInt();
   array2 = remove(number, target);
   String out2 = "The output array is ";
   out2 += listArray(array2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);

   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer = myScanner.next();
  } while (answer.equals("Y") || answer.equals("y"));
 }

 public static String listArray(int number[]) {
  String out = "{";
  for (int j = 0; j < number.length; j++) {
   if (j > 0) {
    out += ", ";
   }
   out += number[j];
  }
  out += "} ";
  return out;
 }



 public static int[] randomInput() { //start of method for random input
  int[] newarray = new int[10]; //start of array that has space for 10 elements
  for (int i = 0; i < newarray.length; i++) { //start of for loop that will count the nubmers inside the array
   newarray[i] = (int)(Math.random() * 10); //this will randomnize the intgers

  }
  return newarray; //returning this new randomized array
 }

 public static int[] delete(int[] list, int number) { //start of delete method
  int[] newArray = new int[list.length - 1]; //this new array is the index of the list array
  int count = 0; 
  int i = 0;
  while (i < list.length) { //while loop that works onlf if the number in the array is less than the numbers in the list array
   if (i == number) { //if statement that states that if that number is equal to the integer number
    continue; //then continue and do this again
   }
   i++; //this is for the while loop to count the numbers in the array
   newArray[count] = list[i];
   count++;
  }
  return newArray;

 }

 public static int[] remove(int[] list, int target) { //the remove method

  int count = 0;

  for (int i = 0; i < list.length; i++) { //for loop that counts the integers insie the list array
   
   if (list[i] == target) { //if statement stating if the the numbers inside the list array are equal to the target
    count++;
   }
  }
  int[] newArray = new int[list.length - count]; //this array equals the array list minus the count
  int count2 = 0;
  for (int i = 0; i < list.length; i++) {
   if (list[i] != target) {
    newArray[count2] = list[i];
    count2++;
   }
  }
  return newArray;
 }


} //endofclass