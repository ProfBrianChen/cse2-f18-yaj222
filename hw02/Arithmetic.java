//////////
/// CSE 02 WelcomeClass
/// Yamelin Jaquez
///September 12, 2018

public class Arithmetic{
  
  public static void main(String args []){
  //start of main method for java 
    
    int numPants = 3; //number of pants
    double pantsPrice = 34.98; //cost of each pair of pants
    int numShirts = 2; //number of shirts
    double shirtsPrice = 24.99; //cost of each shirt
    int numBelts = 1; //number of belts
    double beltCost = 33.99; //cost of each belt
    double paSalesTax = 0.06; //sales tax rate in state of Pennsylvania
    
    double totalCostofPants; //declaring variable for total cost of pants
    double totalCostofShirts;//declaring variable for total cost of shirts
    double totalCostofBelts; //declaring variable for total cost of belts

    totalCostofPants = numPants * pantsPrice; //calculating total cost of pants
    totalCostofShirts = numShirts * shirtsPrice; //calculating total cost of shirts
    totalCostofBelts = numBelts * beltCost;//calculating total cost of belts
    
    System.out.println(" The total cost of pants $" + totalCostofPants);
    //outputting the total cost of pants
    System.out.println(" The total cost of shirts $" + totalCostofShirts);
    //outputting the total cost of shirts
    System.out.println(" The total cost of belts $" + totalCostofBelts);
    //outputting the total cost of belts

    double totalTaxofPants; //declaring variable for taxed amount of pants
    double totalTaxofShirts;//declaring variable for taxed amount of shirts
    double totalTaxofBelts;//declaring variable for taxed amount of belts
      
    totalTaxofPants = ((int)(totalCostofPants * paSalesTax * 100)) / 100.0; 
    //calculating the total tax of pants by multiplying total cost by tax and 100, 
     //then dividing by 100 to eliminate extra decimal digits
    totalTaxofShirts = ((int)(totalCostofShirts * paSalesTax * 100)) / 100.0;
    //calculating the total tax of shirts by multiplying total cost by tax and 100, 
     //then dividing by 100 to eliminate extra decimal digits
    totalTaxofBelts = ((int)(totalCostofBelts * paSalesTax * 100)) / 100.0;
    //calculating the total tax of belts by multiplying total cost by tax and 100, 
     //then dividing by 100 to eliminate extra decimal digits
    
    System.out.println("The total cost of pants with tax is $" + totalTaxofPants);
    //outputting total cost of pants with tax
    System.out.println("The total cost of pants with tax is $" + totalTaxofShirts);
    //outputting total cost of shirts with tax
    System.out.println("The total cost of pants with tax is $" + totalTaxofBelts);
    //outputting total cost of belts with tax
    
    double totalCostofPurchase; //total cost of items before taxes
      
    totalCostofPurchase = totalCostofPants + totalCostofShirts + totalCostofBelts;
    //calculating the total amount of the purchase without taxes by adding items total cost
    
    System.out.println("The total cost of purchase is $" + totalCostofPurchase);
    //outputting the total cost of purchase
    
    double totalSalesTax; //the total tax of the sales
    
    totalSalesTax = ((int)(totalCostofPurchase * paSalesTax *100)) / 100.0;
    //calculating the total salestax by multiplying the total cost of the purchase by 
     //PA tax and 100, then dividing by 100 to reduce decimal digits
    
    System.out.println("The total sales tax is $" + totalSalesTax);
    //outputting the total sales tax of the items
    
    double totalTransaction;
    
    totalTransaction = totalSalesTax + totalCostofPurchase; //calculation for the total transaction after sales tax
      //has been added 
    
    System.out.println("The total amount of transaction is $" + totalTransaction);
     //outputting the total amount of transaction 
     
    
  } //end of main method for java
  
} //end of class 
    
