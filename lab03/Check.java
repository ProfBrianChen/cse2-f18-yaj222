//////////
/// CSE 02 WelcomeClass
/// Yamelin Jaquez
///September 14, 2018
///Determining how much each person pays at dinner using a Scanner class

import java.util.Scanner;
  
  public class Check{
  
     public static void main(String args []){
      //start of main method for java
     Scanner myScanner = new Scanner( System.in );
       //declaring the instance of Scanner 
     System.out.print(" Enter the original cost of the check in the form xx.xx" );
       //outputting the user to enter the cost of check
                      
     double checkCost = myScanner.nextDouble(); //declaring the checkcost and then the method
                      
     System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):");
       //outputting statement that allows user to input a number of percantage tip
       
   
     double tipPercent = myScanner.nextDouble(); // declaring the tip percent and method
     tipPercent /= 100; //converting the tip percentage to decimals
      
     System.out.print("Enter the number of people that went to dinner" );
       //outputting the number of people that went to dinner
       
     int numPeople = myScanner.nextInt(); //declaring the num people and method
     double totalCost; //declaring the total cost
     double costPerPerson; //declaring the cost per person
     int dollars, dimes, pennies; // decalring integer of dollars, dimes, and pennies
       
     totalCost = checkCost * (1 + tipPercent); //calculating the total cost by 
       //multiplying the cost of check by the tip percent
     costPerPerson = totalCost / numPeople;// calculating the total cost per 
       //person by dividing the total cost by the number of people
     
     
      dollars=(int)costPerPerson;
      dimes=(int)(costPerPerson * 10) % 10; 
      pennies=(int)(costPerPerson * 100) % 10; 
       
     System.out.println("Each person in the group owes $" + dollars + '.' +  dimes + pennies);
       
                       
       
       
     } //end of main method   
  } //end of class