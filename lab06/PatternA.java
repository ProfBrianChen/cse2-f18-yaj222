//Yamelin Jaquez
//CSE02
//lab06
//October 16, 2018
//Using loops to create a pyramid of numbers, staring from one and then increasing
import java.util.*; //importing all classes for java; includes all types 

public class PatternA {
    public static void main(String[] args) { //start of main method
        Scanner myScanner = new Scanner(System.in);
        int rowInput; //declaring integer for row user input
        System.out.println("Please enter the number of rows between 1 and 10; ");

        while (!myScanner.hasNextInt()) {
            myScanner.next();
            System.out.println("Error. Please enter an integer only: ");
        }
        rowInput = myScanner.nextInt(); //user input for number of rows

        while (rowInput > 10) { //start of while loop for user input and to make sure integer is between 1 & 10
            System.out.println("Invalid number rowInput! ");
            rowInput = myScanner.nextInt();
        }


        for (int i = 1; i <= rowInput; i++) { //start of for loop for numbers inside pyramid

            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }



    }
}//endofmain