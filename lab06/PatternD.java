//Yamelin Jaquez
//CSE02
//lab06
//October 16, 2018
//Using loops to assure user enters correct type inputs for course number


import java.util.*; //importing all classes for java; includes all types 

public class PatternD {
    public static void main(String[] args) { //start of main method

        Scanner myScanner = new Scanner(System.in);
        int rowInput;
        System.out.println("Please Enter an input between 1 and 10: ");
        //start of while loop to assure that the input is between 1 and 10, and the the input is an integer
        while (!myScanner.hasNextInt()) {
            myScanner.next();
            System.out.println("Error. Please enter an integer only: ");
        }
        rowInput = myScanner.nextInt(); //user input for number of rows
      
      while (rowInput > 10) { //start of while loop for user input and to make sure integer is between 1 & 10
            System.out.println("Invalid number rowInput! ");
            rowInput = myScanner.nextInt();
        }


        for (int i = rowInput; i >= 1; i--) {
            for (int j = i; j >= 1; j--) //numbers start from row input

            {
                System.out.print(j + " ");
            }

            System.out.println();
        }
    }
}