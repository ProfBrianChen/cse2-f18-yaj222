//Yamelin Jaquez
//CSE02
//lab06
//October 16, 2018
//Using loops to create a pyramid of numbers, rows decrement while numbers increment

import java.util.*; //importing all classes for java; includes all types 

public class PatternB {
    public static void main(String[] args) { //start of main method

        Scanner myScanner = new Scanner(System.in);
        int rowInput; //declaring the user input for rows
        System.out.println("Please Enter an input between 1 and 10: ");
        //start of while loop to assure the input is between 1 and 10, and that the input is an integer
        while (!myScanner.hasNextInt()) {
            myScanner.next();
            System.out.println("Error. Please enter an integer only: ");
        }
        rowInput = myScanner.nextInt(); //user input for number of rows

        while (rowInput > 10) { 
            System.out.println("Invalid number rowInput! ");
            rowInput = myScanner.nextInt();
        }

        
        for (int i = rowInput; i >= 1; i--) { //row decrements
            for (int j = 1; j <= i; j++) //numbers increment

            {
                System.out.print(j + " ");
            }

            System.out.println();
        }

    }
}