//Yamelin Jaquez
//CSE02
//lab06
//October 12, 2018
//using loops to build a number pyramid

import java.util.*; //importing all classes for java; includes all types 
public class PatternC {
    public static void main(String[] args) { //start of main method    
        Scanner myScanner = new Scanner(System.in);
        int rowInput=0;
        System.out.println("Please Enter an input between 1 and 10: ");
        //start of while loop to assure that the numver is between 1 and 10, and that the input is an integer
        while (!myScanner.hasNextInt()) {
            myScanner.next();
            System.out.println("Error. Please enter an integer only: ");
        }
        rowInput = myScanner.nextInt(); //user input for number of rows
  
      while (rowInput > 10) { //start of while loop for user input and to make sure integer is between 1 & 10
            System.out.println("Invalid number rowInput! ");
            rowInput = myScanner.nextInt();
        }


        for (int i = 1; i <= rowInput; i++) { 
            for (int k = rowInput - 1; k >= i; k--) { //the row spacing decrements
                System.out.print(" ");
            }

            for (int j = i; j >= 1; j--) { //numbers start at the row input number
                System.out.print(j);
            }
            System.out.println();
        }

    }
}