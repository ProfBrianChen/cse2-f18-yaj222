//Yamelin Jaquez
//CSE02
//November 16, 2018
//using arrays and methods to show what the effects of passing arrays as method arguments are

import java.util.*;
import java.util.Arrays;

public class lab09{
    public static void main(String [] args){
        Scanner myScanner = new Scanner (System.in);
  
        int[] array0 = {1,2,3,4,5,6,7,8};
        int[] array1 = copy (array0);
        int[] array2 = copy (array0);
        int[] array3;
        inverter(array0);
        print(array0);
       inverter2(array1);
        print(array1);   
        array3 = inverter2(array2);
        print(array3);  
        }
    
   public static int[] copy (int[] arrayInput){
        int [] array2 =new int[arrayInput.length]; 
            for(int i =0; i < arrayInput.length; i++){
            array2[i]= arrayInput[i];
        }
        return array2;     
   }
   public static void inverter ( int[] array){
       for ( int i =0; i < array.length / 2; i++){
        int temp =0;
        temp = array[i];
        array[i]= array[array.length-i -1 ];
        array[array.length-i -1 ] = temp;
       }
    }

    public static int[] inverter2(int[] array){
    int [] newArray = copy(array);
    for ( int i =0; i < array.length / 2; i++){
        int temp =0;
        temp = newArray[i];
        newArray[i]= newArray[newArray.length-i -1 ];
        newArray[newArray.length-i -1 ] = temp;
       }

        return newArray;
    }
    public static void print(int[] array){
        for (int i =0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println();


    }
}
    

   
