//Yamelin Jaquez
//CSE02 
//Homework 7
//October 30, 2018
//Lab consits of using methods to let user chose in menu different instances for the text

import java.util.*; //importing all Java Classes
public class Homework7 { //start of class
    //start of main method
    public static void main(String[] args) {
        //declaring the scanner for the user input
        Scanner myScanner = new Scanner(System.in);
        //declaring the user text string that will allow the uset to input text
        //the sample text is the method that the user will enter the text
        String userText = sampleText();
        //printing out the text the user inputs
        System.out.println(userText);
        //declaring user option giving the user the options to make instances to the text
        //print menu will be returned inside a method that will show the different instances of the menu
        String userOption = printMenu();

        //start of switch statement to show the various options the user has to pick from
        switch (userOption) {
            //this is if the option the user picked was c, which will generate the number of non white space characters in text
            case "c":
                int countNumOfNonWSCharacters = getNumofNonWSCharacters(userText); //there is a method that solves this
                System.out.println("Number of non-whitespace characters: " + countNumOfNonWSCharacters);
                break;
                //this is if the option the user picked was w, which will generate the number of words in the text  
            case "w":
                int countWords = getNumOfWords(userText); //there is a method that solves this
                System.out.println("Number of words: " + countWords);
                break;
                //this is if the option the user picked was f, which will outprint the word the user wants to find
            case "f":
                System.out.println("Enter word or phrase to be found: ");
                //asking the user to enter the word
                String findInText = myScanner.nextLine();
                int findInTextCount = findText(userText, findInText); //there is a method that find the word
                System.out.println("Number of words: " + findInTextCount);
                break;
                //this is if the option the user picked was r, which will replace the exclamation points
            case "r":
                String replacedText = replaceExclamation(userText); //there is a method that does this
                System.out.println("The new text is : " + replacedText);
                break;
                //this is if the option the user picked was s, which will shorten the space between words
            case "s":
                String replacedTextSpace = shortenSpace(userText); //there is a method for this
                System.out.println("The new text is: " + replacedTextSpace);
                break;
                //this is if the option the user picked was q, which will quit the program
            case "q":
                System.out.println("Exit the program");
                System.exit(0);
        }

    }
    //start of the method for user to add the text
    public static String sampleText() {
        //declaring the scanner for the user input
        Scanner myScanner = new Scanner(System.in);
        //declaring user text as a String in this method
        String userText; 
        System.out.println("Please enter a sample text: ");
        //saving the response
        userText = myScanner.nextLine();
        //letting the user know what their output was
        System.out.println("Here is your text: " + userText);
        //returning the user text so that it can be used in the main method
        return userText;
    }

    //start of method to print out the menu and its significance
    public static String printMenu() {
        //declaring for user input
        Scanner myScanner = new Scanner(System.in);
        //outprinting what the menu has to offer for the next few lines
        System.out.println("MENU");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
        //prompting the user to choose option
        System.out.println("Choose an option:");
        //saving the input
        String userOption = myScanner.nextLine();
        //returning the user option to the main method
        return userOption;

    }
    //start of method to get the number of white spaces in the text
    public static int getNumofNonWSCharacters(String userText) {
        //declaring interger to count, initializng it to 0
        int getWSCount = 0;
        //start of for loop to check the text and present back the amount of spaces the loop counted
        //(length) returns the length of the string
        for (int i = 0; i < userText.length(); i++) { //i will help count 
            //start of if statement that checks when the loop reaches a space ' ' after a character
            if (userText.charAt(i) == ' ') {
            // this will count how many spaces are at the end
                getWSCount++;
            }
        }//return value for numbeer of spaces, will be used in main method
        return getWSCount;
    }
    //start of method to get the number of words in the text
    public static int getNumOfWords(String userText) {
        //declaring interger to the count the words
        int getWordCount = 0;
        //start of for loop that will return the length of the text 
        // the length of the text is subtracted by one because the if will count backwards from the spaces
        for (int i = 0; i < userText.length() - 1; i++) {
            //start of if statement that states that if the character in counting is not a space, 
            //and the character after counting is a space, then that is a word
            if (userText.charAt(i) != ' ' && userText.charAt(i + 1) == ' ');
            //if true, the this is a word therefor it will count the words
            getWordCount++;
        } //return the number of words to main method
        return getWordCount;
    }
    //start of method to find a a word in the the text
    public static int findText(String userText, String findWord) {
        //declaring an integer to count to find the word
        int findCount = 0;
        //for loop to go through the length of the text and find the desired word
        for (int i = 0; i < userText.length() - findWord.length() + 1; i++) {
            //the substring will begin at the desired  text index and will extend until word is found
            String current = userText.substring(i, i + findWord.length());
            //if statement says that if the word equaks to the current index, and there is character
            //in the sqeuence that is a space, then that is the desired word
            if (current.equals(findWord) && userText.charAt(i + findWord.length()) == ' ') {
                findCount++;
            }
        }
        //returning find count after user has found the word, this will return to the main method
        return findCount;
    }
    //start of method to replace the exclamation point for a period in the text
    public static String replaceExclamation(String userText) {
        //declaring the string change
        //replace all is a mehtod that will replace the given substring to one that mathces the literal target
        String change = userText.replaceAll("!", ".");
        //returning that change
        return change;
    }
    //start of method to shrorten the space between to words/characters
    public static String shortenSpace(String userText) {
        //declaring string for the new text after the spaces
        String newText = " ";
        //start of for loop that will go through the lenght of the text 
        for (int i = 0; i < userText.length(); i++) {
            //if a character equals to a space, and if the next character after that is also a space, 
            if (userText.charAt(i) == ' ' && userText.charAt(i + 1) == ' ');
            //then the new next will delete that space 
            newText = userText.substring(0, i) + " " + userText.substring(i + 1, userText.length());
        }
        return newText;
    }

}//end of class