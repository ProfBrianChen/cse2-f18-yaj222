//////////
/// CSE02 
/// Yamelin Jaquez 
/// Septemeber 7, 2018
/// Measuring elapsed time and rotations of front wheels in a bicycle. 

  public class Cyclometer{
    // start of main method for java
  
   public static void main(String args []){
     
     int secsTrip1 = 480; // number of seconds in the first trip
     int secsTrip2 = 3220; // number of seconds in the second trip
     int countsTrip1 = 1561; // number of rotations in the frist trip
     int countsTrip2 = 9037; // number of rotations in the second trip
     
     double wheelDiameter = 27.0; // the diameter of the bicyle's wheel
     double PI = 3.14159; // ratio of the circumference of a circle to it's diameter
     int feetPerMile = 5280; // how many feet are in one mile
     int inchesPerFoot = 12; // how many inches in one foot
     int secondsPerMinute = 60; // the amount of seconds in one minute
     double distanceTrip1; // the distance for Trip1
     double distanceTrip2; //the distane for Trip2 
     double totalDistance; // the total distance of Trip1 and Trip 2
       
   System.out.println("Trip1 took " + (secsTrip1 / secondsPerMinute) + 
                      "minutes and had " + countsTrip1 + " counts.");
     // outputting the number of minutes Trip1 took and how many counts too 
   System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
     // outputting the number minutes Trip2 took and how many counts too
     
 
     
    distanceTrip1 = countsTrip1* wheelDiameter * PI; 
     // the total distance of ditanceTrip1 using the counts of the Trip1,wheelDiameter, and PI
    distanceTrip1 = inchesPerFoot * feetPerMile;
     // the total distane of distanceTrip1 using inches in a foor and feet per mile
    distanceTrip2 = countsTrip2 * wheelDiameter * PI;
     // the total ditance of trip2 multiplying counts of trip2, wheeel diameter and PI
    totalDistance = distanceTrip1 + distanceTrip2;
     
  System.out.println("Trip 1 was " + distanceTrip1 + " miles");
     // outputting the distance in miles of Trip1
	System.out.println("Trip 2 was " + distanceTrip2 + " miles");
     // outputting the distance in miles of Trip2
	System.out.println("The total distance was " + totalDistance + " miles");
     // outputtong the total distance of both Trip1 and Trip2
  
       
                       
       }
    
    
   }