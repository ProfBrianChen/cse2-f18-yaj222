///CSE02
///Yamelin Jaquez
///September 21, 2018
//Ouputting Casino game called Craps, using user input and random numbers and switch stamennts.

import java.util.Scanner; 
//importing scanner class
import java.util.Random;
//importing random class

public class CrapsSwitch {
    public static void main(String[] args) {
      //start of main method
        Scanner myScanner = new Scanner(System.in); //declaring my scanner for later user input
        System.out.println("Indicate 1 to randomly cast dice. Indicate 2 to state the two dice numbers:");
         //outputting decison for user, 1 will create random generator for the two dice; 2 will allow user to input value of dice
        int userDecision = myScanner.nextInt(); //declaring variable for user decision
        int numberDice1 = 0; //declaring number of one dice
        int numberDice2 = 0; //declaring number of another dice
        String output = ""; //declaring string for output of the roll of the dice
        switch (userDecision) { //start of switch for the user decision

            case 1: //case 1 refers to user decison 1 of using random numbers
                Random random = new Random(); ///declaring random to randomize dice numbers
                numberDice1 = random.nextInt(6) + 1;//declaring one dice, numbers 1-6
                numberDice2 = random.nextInt(6) + 1;//declaring one dice, numbers 1-6
                System.out.println("Here is dice 1: \n" + numberDice1 + " \nHere is dice 2\n" + numberDice2); //outputting the dice numbers for user to see
                break; //break for first case

            case 2: //case 2 refers to user decision 2 of inputting value of dice
                System.out.println("Enter: "); //outputting for user to put one dice value number 
                numberDice1 = myScanner.nextInt(); //dice one asking for user input
                System.out.println("Enter: ");//outputting for user to put another dice value number 
                numberDice2 = myScanner.nextInt(); //dice 2 asking for user inputting

        }//end of switch for user decision

        switch (numberDice1) { //start of switch statement for roll of dice number one
            case 1: //case 1 refers to if the dice number is 1
                switch (numberDice2) { //start of switch roll of dice number 2
                    case 1: //case 1 refers to the output of the roll of the dice, in this case #1
                        output = "snake eyes"; //output of snake eyes; the sum of case 1 with 1
                        break;
                    case 2: //case 2 refers to if the dice number output is 2
                        output = "ace deuce";//output of ace deuce; the sum of case 1 with 2
                        break;
                    case 3://case 3 refers to if the dice number output is 3
                        output = "easy four";//output of easy four; the sum of case 1 with case 3 
                        break;
                    case 4: //case 4 refers to if the dice number output is 4
                        output = "fever five";  //output of fever five ; the sum of case 1 with case 4
                        break;
                    case 5: //case 5 refers to if the dice number output is 5
                        output = "easy six"; //output of easy six; the sum of case 1 with case 5 
                        break;
                    case 6: //case 6 refers to if the dice number output is 6
                        output = "seven out"; ///output of seven out; the sum of case 1 with case 6
                        break;
                } //endof switch of roll of dice number 2
                break;
            case 2://case 1 refers to if the dice number ouput is 2
                switch (numberDice2) {//start of switch roll of dice number 2
                    case 1://case 1 refers to the output of the roll of the dice, in this case #1
                        output = "ace deuce";//output of ace deuce; the sum of case 1 with 2
                        break;
                    case 2://case 2 refers to if the dice number output is 2
                        output = "hard four";//output of hard four; the sum of case 2 with 2
                        break;
                    case 3://case 3 refers to if the dice number output is 3
                        output = "fever five";//output of fever five; the sum of case 2 with 2
                        break;
                    case 4://case 4 refers to if the dice number output is 4
                        output = "easy six";//output of easy six; the sum of case 2 with 4
                        break;
                    case 5://case 5 refers to if the dice number output is 5
                        output = "seven out";//output of seven out; the sum of case 2 with 5
                        break;
                    case 6://case 6 refers to if the dice number output is 6
                        output = "easy eight";//output of easy eight; the sum of case 2 with 6
                }
                break;
            case 3://case 3 refers to the output of the roll of the dice, in this case #3
                switch (numberDice2) {
                    case 1: //case 1 refers to the output of the roll of the dice, in this case #1
                        output = "easy four";//output of easy four; the sum of case 1 with 3
                        break;
                    case 2://case 2 refers to if the dice number output is 2
                        output = "fever five";//output of fever five; the sum of case 3 with 2
                        break;
                    case 3://case 3 refers to if the dice number output is 3
                        output = "hard six";//output of hard six; the sum of case 3 with 3
                        break;
                    case 4://case 4 refers to if the dice number output is 4
                        output = "seven out";//output of seven out; the sum of case 3 with 4
                        break;
                    case 5://case 5 refers to if the dice number output is 5
                        output = "easy eight";//output of easy eight; the sum of case 3 with 5
                        break;
                    case 6://case 6 refers to if the dice number output is 6
                        output = "nine";//output of nine; the sum of case 3 with 6
                }
                break;
            case 4://case 4 refers to the output of the roll of the dice, in this case #4
                switch (numberDice2) {
                    case 1: //case 1 refers to the output of the roll of the dice, in this case #1
                        output = "fever five";//output of fever five; the sum of case 1 with 4
                        break;
                    case 2://case 2 refers to if the dice number output is 2
                        output = "easy six";//output of easy six; the sum of case 4 with 2
                        break;
                    case 3://case 3 refers to if the dice number output is 3
                        output = "seven out";//output of seven out; the sum of case 4 with 3
                        break;
                    case 4://case 4 refers to if the dice number output is 4
                        output = "hard eight";//output of hard eight; the sum of case 4 with 4
                        break;
                    case 5://case 5  refers to if the dice number output is 5
                        output = "nine";//output of nine; the sum of case 4 with 5
                        break;
                    case 6://case 6 refers to if the dice number output is 6
                        output = "easy ten";//output of easy ten; the sum of case 4 with 6
                }
                break;
            case 5://case 5 refers to the output of the roll of the dice, in this case #5
                switch (numberDice2) {
                    case 1: //case 1 refers to the output of the roll of the dice, in this case #1
                        output = "easy six";//output of easy six; the sum of case 1 with 5
                        break;
                    case 2://case 2 refers to if the dice number output is 2
                        output = "seven out";//output of seven out; the sum of case 5 with 2
                        break;
                    case 3://case 3 refers to if the dice number output is 3
                        output = "easy eight";//output of easy eight; the sum of case 5 with 3
                        break;
                    case 4://case 4 refers to if the dice number output is 4
                        output = "nine";//output of nine; the sum of case 5 with 4
                        break;
                    case 5://case 5 refers to if the dice number output is 5
                        output = "hard ten";//output of hard ten; the sum of case 5 with 5
                        break;
                    case 6://case 6 refers to if the dice number output is 6
                        output = "Yo-leven";//output of Yo-leven; the sum of case 5 with 6
                }
                break;
            case 6://case 6 refers to the output of the roll of the dice, in this case #6
                switch (numberDice2) {
                    case 1: //case 1 refers to the output of the roll of the dice, in this case #1
                        output = "seven out";//output of seven out; the sum of case 1 with 6
                        break;
                    case 2://case 2 refers to if the dice number output is 2
                        output = "easy eight";//output of easy eight; the sum of case 6 with 2
                        break;
                    case 3://case 3 refers to if the dice number output is 3
                        output = "nine";//output of nine; the sum of case 6 with 3
                        break;
                    case 4://case 4 refers to if the dice number output is 4
                        output = "easy ten";//output of easy ten; the sum of case 6 with 4
                        break;
                    case 5://case 5 refers to if the dice number output is 5
                        output = "Yo-leven";//output of Yo-leven; the sum of case 6 with 5
                        break;
                    case 6://case 6 refers to if the dice number output is 6
                        output = "Boxcars";//output of Boxcars; the sum of case 6 with 6
                }

        }
        System.out.println("The dice roll is " + output); //ouputting the name of the dice roll


    } 


}//end of main method

