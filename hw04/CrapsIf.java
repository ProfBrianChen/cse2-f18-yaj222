///CSE02
///Yamelin Jaquez
///September 21, 2018
//Ouputting Casino game called Craps, using user input and random numbers and if statements.

import java.util.Scanner; 
//importing scanner class
import java.util.Random;
//importing random class


public class CrapsIf{
    public static void main(String[] args) {
      //start of main method
        Scanner myScanner = new Scanner(System.in); //declaring my scanner for later user input
        System.out.println("Indicate '1' to randomly cast dice. Indicate '2' to state the two dice:"); 
         //outputting decison for user, 1 will create random generator for the two dice; 2 will allow user to input value of dice
        int userDecision = myScanner.nextInt(); //declaring variable for user decision

        if (userDecision == 1) { //start of if statement for decision to randomly cast the dice
            Random random = new Random(); //declaring random to randomize dice numbers
            int numberDice1 = random.nextInt(6) + 1; //declaring one dice, numbers 1-6
            int numberDice2 = random.nextInt(6) + 1; //declaring other dice, numbers 1-6
            System.out.println("Here is dice 1: \n" + numberDice1 + " \nHere is dice 2\n" + numberDice2);  //outputting the dice numbers for user to see
            System.out.println("Slang Terminology: "); //output slang after dice numbers have been found
        


            if (numberDice1 == 1 && numberDice2 == 1) { //start of if statement if the dice numbers are 1 and 1
                System.out.println("Snake Eyes"); //ouputting snake eyes
            } else if (numberDice1 == 1 && numberDice2 == 2) { //start of if statement if the dice numbers are 1 and 2
                System.out.println("Ace Deuce"); //outputting ace deuce
            } else if (numberDice1 == 1 && numberDice2 == 3) {//start of if statement if the dice numbers are 1 and 3
                System.out.println("Easy Four"); //outputting easy four
            } else if (numberDice1 == 1 && numberDice2 == 4) {//start of if statement if the dice numbers are 1 and 4
                System.out.println("Fever Five"); //outputting fever five
            } else if (numberDice1 == 1 && numberDice2 == 5) {//start of if statement if the dice numbers are 1 and 5
                System.out.println("Easy Six"); //outputting easy six
            } else if (numberDice1 == 1 && numberDice2 == 6) {//start of if statement if the dice numbers are 1 and 6
                System.out.println("Seven out"); //outputting seven out
            } else if (numberDice1 == 2 && numberDice2 == 2) {//start of if statement if the dice numbers are 2 and 2 
                System.out.println("Hard Four"); //outputting hard four
            } else if (numberDice1 == 2 && numberDice2 == 3) {//start of if statement if the dice numbers are 2 and 3
                System.out.println("Fever five"); //outputting fever five
            } else if (numberDice1 == 2 && numberDice2 == 4) {//start of if statement if the dice numbers are 2 and 4
                System.out.println("Easy six"); //outputting easy six
            } else if (numberDice1 == 2 && numberDice2 == 5) {//start of if statement if the dice numbers are 2 and 5
                System.out.println("Seven out"); //outputtingputting seven out
            } else if (numberDice1 == 2 && numberDice2 == 6) {//start of if statement if the dice numbers are 2 and 6
                System.out.println("Easy eight"); //outputting easy eight
            } else if (numberDice1 == 3 && numberDice2 == 3) {//start of if statement if the dice numbers are 3 and 3
                System.out.println("Hard six"); //outputting hard six
            } else if (numberDice1 == 3 && numberDice2 == 4) {//start of if statement if the dice numbers are 3 and 4
                System.out.println("Seven out"); //outputting seven out
            } else if (numberDice1 == 3 && numberDice2 == 5) {//start of if statement if the dice numbers are 3 and 5
                System.out.println("Easy eight"); //outputting easy eight
            } else if (numberDice1 == 3 && numberDice2 == 6) {//start of if statement if the dice numbers are 3 and 6
                System.out.println("Nine"); //outputting nine
            } else if (numberDice1 == 4 && numberDice2 == 4) {//start of if statement if the dice numbers are 4 and 4
                System.out.println("Hard Eight"); //oututting hard eight
            } else if (numberDice1 == 4 && numberDice2 == 5) {//start of if statement if the dice numbers are 4 and 5
                System.out.println("Nine"); //outputting nine
            } else if (numberDice1 == 4 && numberDice2 == 6) {//start of if statement if the dice numbers are 4 and 6
                System.out.println("Easy Ten"); //outputting easy ten
            } else if (numberDice1 == 5 && numberDice2 == 5) {//start of if statement if the dice numbers are 5 and 5
                System.out.println("Hard Ten"); //outputting hard ten
            } else if (numberDice1 == 5 && numberDice2 == 6) {//start of if statement if the dice numbers are 5 and 6
                System.out.println("Yo-Leven"); //outputting yo leven
            } else if (numberDice1 == 6 && numberDice2 == 6) {//start of if statement if the dice numbers are 6 and 6
                System.out.println("Boxcars"); //outputting box cars

            } 
        } //end of if statement pertaining to the both dice equaling 1 


        //how do I provide an integer within range? 1-6                         
        if (userDecision == 2) { //start of if statement if the user decision is that they want to state the dice numbers
            System.out.println("Please enter dice number for dice 1: "); //ouputting for user to enter dice number
            int dice1 = myScanner.nextInt(); //declaring dice 1 for user input
            System.out.println("Please enter dice number for dice 2: ");//ouputting for user to enter dice number
            int dice2 = myScanner.nextInt();//declaring dice 2 for user input
            System.out.println("Slang Terminology: "); //ouput sland for the result of the dice numbers 


            if (dice1 == 1 && dice2 == 1) {//start of if statement if the dice numbers are 1 and 1
                System.out.println("Snake Eyes");//outputting snake eyes
            } else if (dice1 == 1 && dice2 == 2) {//start of if statement if the dice numbers are 1 and 2
                System.out.println("Ace Deuce"); //outputting ace deuce
            } else if (dice1 == 1 && dice2 == 3) {//start of if statement if the dice numbers are 1 and 3
                System.out.println("Easy Four"); //outputting easy four
            } else if (dice1 == 1 && dice2 == 4) {//start of if statement if the dice numbers are 1 and 4
                System.out.println("Fever Five"); //outputting fever five
            } else if (dice1 == 1 && dice2 == 5) {//start of if statement if the dice numbers are 1 and 5 
                System.out.println("Easy Six"); //outputting easy six
            } else if (dice1 == 1 && dice2 == 6) {//start of if statement if the dice numbers are 1 and 6
                System.out.println("Seven out"); //outputting seven out
            } else if (dice1 == 2 && dice2 == 2) {//start of if statement if the dice numbers are 2 and 2
                System.out.println("Hard Four"); //outputting hard four
            } else if (dice1 == 2 && dice2 == 3) {//start of if statement if the dice numbers are 2 and 3
                System.out.println("Fever five"); //outputting fever five
            } else if (dice1 == 2 && dice2 == 4) {//start of if statement if the dice numbers are 2 and 4
                System.out.println("Easy six"); //outputting easy six
            } else if (dice1 == 2 && dice2 == 5) {//start of if statement if the dice numbers are 2 and 5
                System.out.println("Seven out"); //outputting seven out
            } else if (dice1 == 2 && dice2 == 6) {//start of if statement if the dice numbers are 2 and 6
                System.out.println("Easy eight"); //outputting easy eight
            } else if (dice1 == 3 && dice2 == 3) {//start of if statement if the dice numbers are 3 and 3
                System.out.println("Hard six"); //outputting hard six
            } else if (dice1 == 3 && dice2 == 4) {//start of if statement if the dice numbers are 3 and 4
                System.out.println("Seven out"); //outputting seven out
            } else if (dice1 == 3 && dice2 == 5) {//start of if statement if the dice numbers are 3 and 5
                System.out.println("Easy eight"); //ouputting easy eight
            } else if (dice1 == 3 && dice2 == 6) {//start of if statement if the dice numbers are 3 and 6
                System.out.println("Nine"); //outputting nine
            } else if (dice1 == 4 && dice2 == 4) {//start of if statement if the dice numbers are 4 and 4
                System.out.println("Hard Eight"); //ouputting hard eight
            } else if (dice1 == 4 && dice2 == 5) {//start of if statement if the dice numbers are 4 and 5
                System.out.println("Nine"); //ouputting nine
            } else if (dice1 == 4 && dice2 == 6) {//start of if statement if the dice numbers are 4 and 6
                System.out.println("Easy Ten"); //outputting easy ten
            } else if (dice1 == 5 && dice2 == 5) {//start of if statement if the dice numbers are 5 and 5
                System.out.println("Hard Ten"); //outputting hard ten
            } else if (dice1 == 5 && dice2 == 6) {//start of if statement if the dice numbers are 5 and 6
                System.out.println("Yo-Leven"); //outputting yo leven
            } else if (dice1 == 6 && dice2 == 6) {//start of if statement if the dice numbers are 6 and 6
                System.out.println("Boxcars"); //ouputting box cars
            }
        }//end of if statement pertaining to the both dice equaling 1 
    }

} //end of main method