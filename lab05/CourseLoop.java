//Yamelin Jaquez
//CSE02
//lab05
//October 5, 2018
//Using while loops to assure user enters  correct type inputs for course number, department name
///(continued) the amount ot times user has the class a week, the instructor name, and the number of students

import java.util.*; //importing all classes for java; includes all types 

public class CourseLoop {
    public static void main(String[] args) { //start of main method

        Scanner myScanner = new Scanner(System.in); //declaring system for user input 

        int courseNum; //declaring course number as an interger
        String departmentName; //declaring the departmeny name as string
        int timesWkClass; //declaring the times a week as an integer
        int classTime; //declaring the class time as an integer
        String instrcName; //declaring the instructo's name as string
        int numStudents; //delcaring the number of students as an integer

        System.out.println("Enter the course number: "); //asking the user for input for course number as an integer
        while (!myScanner.hasNextInt()) { //start of loop to check that the input is an integer
            myScanner.next();
            System.out.println("Error: Please enter a course number "); //stating error; asking user for integer input 
        }
        courseNum = myScanner.nextInt(); //indicating that the integer is the course number

        System.out.println("Enter the department name: "); //asking the user for input for course number as a string
        while (!myScanner.hasNext()) { //start of loop to check that the user inputs a string
            myScanner.next();
            System.out.println("Error: Please enter a deparment name "); //stating error; asking user for string input
        }
        departmentName = myScanner.next(); //indicating that the string, is the department name

        System.out.println("Enter the times a week you have the class: "); //asking the user for input for course number as an integer
        while (!myScanner.hasNextInt()) { //start of loop to check that the input is an integer
            myScanner.next();
            System.out.println("Error: Please enter the number of times a week you have the class "); //stating error; asking user for integer input
        }
        timesWkClass = myScanner.nextInt(); //indicating that the integer, is the times a week 

        System.out.println("Enter when the class starts: "); //asking the user for input for course number as an integer
        while (!myScanner.hasNextInt()) { //start of loop to check that the input is an integer
            myScanner.next();
            System.out.println("Error: Please enter the class time "); //stating error; asking user for integer input
        }
        classTime = myScanner.nextInt();
        indicating that the integer is the

        System.out.println("Enter the instructor name: "); //asking the user for input for course number as an integer
        while (!myScanner.hasNext()) { //start of loop to check that user input is a string
            myScanner.next();
            System.out.println("Error: Please enter instructor name "); //stating error; asking user for string input
        }
        instrcName = myScanner.next(); //indicating that the string is the intructor name

        System.out.println("Enter the number of students in class: "); //asking the user for input for course number as an integer
        while (!myScanner.hasNextInt()) { //start of loop to check that the input is an integer
            myScanner.next();
            System.out.println("Error: Please enter the number of students "); //stating error; asking user for integer input
        }
        numStudents = myScanner.nextInt(); //indicating that the integer is the the number of students


        System.out.println("The course number is: " + courseNum); //outputting the coursenumber
        System.out.println("The deparment name is:  " + departmentName); //ouputting the departmentname
        System.out.println("The times a week for the class is: " + timesWkClass); //ouputting the times a week for the class
        System.out.println("The class time is: " + classTime); //ouputting the class time
        System.out.println("The instructor's name: " + instrcName); //ouputting the instructors name
        System.out.println("The number of students: " + numStudents); //ouputting the number of students

    }
} //end of main method