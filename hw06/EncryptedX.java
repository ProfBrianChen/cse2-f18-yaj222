
///Yamelin Jaquez
///CSE02
///October 18, 2018
///Printing out a pattern with an hidden X in the middle hidden between stars. This is done using loops/nested loops.

import java.util.*; //importing all classes for java; includes all types 

public class EncryptedX {
    public static void main(String[] args) { // start of main method
        Scanner myScanner = new Scanner(System.in); // setting up the input scanner
        int sqrInput; // declaring integer for user input; this will determine the size of grid
        System.out.println("Please enter the number of the square between 0 and 100; "); // asking the user for input

        while (!myScanner.hasNextInt()) { // start of while loop to ensure that that user is inputing an integer
            myScanner.next();
            System.out.println("Error. Please enter an integer only: "); // error message
        }
        sqrInput = myScanner.nextInt(); // end of while loop that allows user to input the integer

        while (sqrInput > 100) { // start of while loop for user input and to make sure integer is between 0 &
           // 100
            System.out.println("Invalid number for Square size Input! "); // error message if user inputs a number
            // greater than 100
            sqrInput = myScanner.nextInt();// end of while loop that allows user to enter integer
        }

        int checkNum = 1; // integer will be used to check to make sure that the number is even or odd
        int checkNum2 = 0;// other integer that will also be used to check to make sure that the number is
             // even or odd

        // start of if statement that determines if the user input is even
        if (sqrInput % 2 == 0) { // condition check if the input is divisible by two, if the answer is 0, then it is even
           
            checkNum = 0; // this will state that checkNum is even, meaning that the input is even
            checkNum2 = 1; // the number is not divided by 2,  this will state that the input number we are checking is odd

        }
        //start of for loop to determine the rows, columns, and spaces of the program
        for (int i = 0; i < sqrInput; i++) { //condition declares new variable i equal to 0, this will start the rows of the program
            //the new varible must be smaller than the square input, this will check it won't go past that input
            //i will increment after each loop before it reaches the input
            
            //start of nested for loop for the columns
            for (int k = 0; k < sqrInput + checkNum2; k++) { //conditon declares a new variable k equal to 0
                //the new variable must be smaller than the square input added with the checkNum2 (odd)
                //I added the sqrInput by checkNum2 because it will make sure that the odd number is included 
            
                //start of if statement to check that if the sequence reaches one of the corners, it will print a space
                if (i == k || ((i + checkNum) == (sqrInput - k))) { //if i is equal to K, meaning the start of the sequence (rows & columns)
                    //or if the start of the rows added with checkNum(even) equals to the inpout minus the start of columns
                    System.out.print(" "); //print out a space
                } else {
                    System.out.print("*"); //print out a star
                }
            }

            System.out.println();
        }
    }

} //end of class
