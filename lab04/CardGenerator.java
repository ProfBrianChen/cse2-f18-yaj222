///CSE02
///Yamelin Jaquez
/// Septembereptember 21, 2018
///Picking a random card for the magician trick
import java.util.Random;

public class CardGenerator {
    public static void main(String[] args) {
      //start of main method

        int cardNum; //declaring variable for numcber of card
        cardNum = (int)(Math.random() * 52) + 1; //generates a random number between 1 and 52 and casts it as an integer


        if (cardNum == 1) {
            System.out.println("You picked the Ace of Diamonds"); //if the random number is a 1, then prints out the line of text for diamonds
        } else if (cardNum == 2) {
            System.out.println("You picked the 2 of Diamonds"); //if the random number is a 2, then prints out the line of text for diamonds
        } else if (cardNum == 3) {
            System.out.println("You picked the 3 of Diamonds"); //if the random number is a 3, then prints out the line of text for diamonds
        } else if (cardNum == 4) {
            System.out.println("You picked the 4 of Diamonds"); //if the random number is a 4, then prints out the line of text for diamonds
        } else if (cardNum == 5) {
            System.out.println("You picked the 5 of Diamonds"); //if the random number is a 5, then prints out the line of text for diamonds
        } else if (cardNum == 6) {
            System.out.println("You picked the 6 of Diamonds"); //if the random number is a 6, then prints out the line of text for diamonds
        } else if (cardNum == 7) {
            System.out.println("You picked the 7 of Diamonds"); //if the random number is a 7, then prints out the line of text for diamonds
        } else if (cardNum == 8) {
            System.out.println("You picked the 8 of Diamonds"); //if the random number is a 8, then prints out the line of text for diamonds
        } else if (cardNum == 9) {
            System.out.println("You picked the 9 of Diamonds"); //if the random number is a 9, then prints out the line of text for diamonds
        } else if (cardNum == 10) {
            System.out.println("You picked the 10 of Diamonds"); //if the random number is a 10, then prints out the line of text for diamonds
        } else if (cardNum == 11) {
            System.out.println("You picked the Jack of Diamonds"); //if the random number is a 11, then prints out the line of text for diamonds
        } else if (cardNum == 12) {
            System.out.println("You picked the Queen of Diamonds"); //if the random number is a 12, then prints out the line of text for diamonds
        } else if (cardNum == 13) {
            System.out.println("You picked the King of Diamonds"); //if the random number is a 13, then prints out the line of text for diamonds
        } else if (cardNum == 14) {
            System.out.println("You picked the Ace of Clubs"); //if the random number is a 14, then prints out line of text for clubs
        } else if (cardNum == 15) {
            System.out.println("You picked the 2 of Clubs"); //if the random number is a 15, then prints out line of text for clubs
        } else if (cardNum == 16) {
            System.out.println("You picked the 3 of Clubs"); //if the random number is a 16, then prints out line of text for clubs
        } else if (cardNum == 17) {
            System.out.println("You picked the 4 of Clubs"); //if the random number is a 17, then prints out line of text for clubs
        } else if (cardNum == 18) {
            System.out.println("You picked the 5 of Clubs"); //if the random number is a 18, then prints out line of text for clubs
        } else if (cardNum == 19) {
            System.out.println("You picked the 6 of Clubs"); //if the random number is a 19, then prints out line of text for clubs
        } else if (cardNum == 20) {
            System.out.println("You picked the 7 of Clubs"); //if the random number is a 20, then prints out line of text for clubs
        } else if (cardNum == 21) {
            System.out.println("You picked the 8 of Clubs"); //if the random number is a 21, then prints out line of text for clubs
        } else if (cardNum == 22) {
            System.out.println("You picked the 9 of Clubs"); //if the random number is a 22, then prints out line of text for clubs
        } else if (cardNum == 23) {
            System.out.println("You picked the 10 of Clubs"); //if the random number is a 23, then prints out line of text for clubs
        } else if (cardNum == 24) {
            System.out.println("You picked the Jack of Clubs"); //if the random number is a 24, then prints out line of text for clubs
        } else if (cardNum == 25) {
            System.out.println("You picked the Queen of Clubs"); //if the random number is a 25, then prints out line of text for clubs
        } else if (cardNum == 26) {
            System.out.println("You picked the King of Clubs"); //if the random number is a 26, then prints out line of text for clubs
        } else if (cardNum == 27) {
            System.out.println("You picked the Ace of Hearts"); //if the random number is a 27, then prints out line of text for Hearts
        } else if (cardNum == 28) {
            System.out.println("You picked the 2 of Hearts"); //if the random number is a 28, then prints out line of text for Hearts
        } else if (cardNum == 29) {
            System.out.println("You picked the 3 of Hearts"); //if the random number is a 29, then prints out line of text for Hearts
        } else if (cardNum == 30) {
            System.out.println("You picked the 4 of Hearts"); //if the random number is a 30, then prints out line of text for Hearts
        } else if (cardNum == 31) {
            System.out.println("You picked the 5 of Hearts"); //if the random number is a 31, then prints out line of text for Hearts
        } else if (cardNum == 32) {
            System.out.println("You picked the 6 of Hearts"); //if the random number is a 32, then prints out line of text for Hearts
        } else if (cardNum == 33) {
            System.out.println("You picked the 7 of Hearts"); //if the random number is a 33, then prints out line of text for Hearts
        } else if (cardNum == 34) {
            System.out.println("You picked the 8 of Hearts"); //if the random number is a 34, then prints out line of text for Hearts
        } else if (cardNum == 35) {
            System.out.println("You picked the 9 of Hearts"); //if the random number is a 35, then prints out line of text for Hearts
        } else if (cardNum == 36) {
            System.out.println("You picked the 10 of Hearts"); //if the random number is a 36, then prints out line of text for Hearts
        } else if (cardNum == 37) {
            System.out.println("You picked the Jack of Hearts"); //if the random number is a 37, then prints out line of text for Hearts
        } else if (cardNum == 38) {
            System.out.println("You picked the Queen of Hearts"); //if the random number is a 38, then prints out line of text for Hearts
        } else if (cardNum == 39) {
            System.out.println("You picked the King of Hearts"); //if the random number is a 39, then prints out line of text for Hearts
        } else if (cardNum == 40) {
            System.out.println("You picked the Ace of Spades"); //if the random number is a 40, then prints out line of text for spades
        } else if (cardNum == 41) {
            System.out.println("You picked the 2 of Spades"); //if the random number is a 41, then prints out line of text for spades
        } else if (cardNum == 42) {
            System.out.println("You picked the 3 of Spades"); //if the random number is a 42, then prints out line of text for spades
        } else if (cardNum == 43) {
            System.out.println("You picked the 4 of Spades"); //if the random number is a 43, then prints out line of text for spades
        } else if (cardNum == 44) {
            System.out.println("You picked the 5 of Spades"); //if the random number is a 44, then prints out line of text for spades
        } else if (cardNum == 45) {
            System.out.println("You picked the 6 of Spades"); //if the random number is a 45, then prints out line of text for spades
        } else if (cardNum == 46) {
            System.out.println("You picked the 7 of Spades"); //if the random number is a 46, then prints out line of text for spades
        } else if (cardNum == 47) {
            System.out.println("You picked the 8 of Spades"); //if the random number is a 47, then prints out line of text for spades
        } else if (cardNum == 48) {
            System.out.println("You picked the 9 of Spades"); //if the random number is a 48, then prints out line of text for spades
        } else if (cardNum == 49) {
            System.out.println("You picked the 10 of Spades"); //if the random number is a 49, then prints out line of text for spades
        } else if (cardNum == 50) {
            System.out.println("You picked the Jack of Spades"); //if the random number is a 50, then prints out line of text for spades
        } else if (cardNum == 51) {
            System.out.println("You picked the Queen of Spades"); //if the random number is a 51, then prints out line of text for spades
        } else if (cardNum == 52) {
            System.out.println("You picked the King of Spades"); //if the random number is a 52, then prints out line of text for spades
        }

    } //end of main method
} //end of class
