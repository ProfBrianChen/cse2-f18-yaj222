//Yamelin Jaquez 
//CSE02
//October 8, 2018
//Finding out the probablity of each hand, there are five cards that can either be generated to
///One of a kind, two of a kind, three of a kind, or four of a kind

import java.util.*; //importing all classes for java; includes all types 

public class Poker {
    public static void main(String[] args) { //start of main method
        Scanner myScanner = new Scanner(System.in); //importing scanner for user input

        int genrHands = 0; //declaring integer to generate the number of hands (loops)
        int fourofKind = 0;
        int twoofKind = 0;
        int threeofKind = 0;
        int oneofKind = 0;
        //asking user to input the number of times they woudl like to generate hands. Each hand will have five cards
        System.out.println("Enter the number of times you would like to generate hands: ");
        while (!myScanner.hasNextInt()) { //start of loop to check that the input is an integer
            myScanner.next();
            System.out.println("Error: Please enter anumber "); //stating error; asking user for integer input 
        }
        genrHands = myScanner.nextInt(); //indicating the integer value for generating hands

        //start of for loop to indicate the output of random numbers for the cards
        for (int x = 0; x < genrHands; x++) {
            //random number must be between 1 and 52
            double cardOne = (int)((Math.random() * 52) + 1);
            double cardTwo = (int)((Math.random() * 52) + 1);
            double cardThree = (int)((Math.random() * 52) + 1);
            double cardFour = (int)((Math.random() * 52) + 1);
            double cardFive = (int)((Math.random() * 52) + 1);

                // while loop to make sure no cards in the hand are identical; 
                //there can't be identical cards in a deck
            while (cardTwo == cardOne) {
                //if card two is equal to card one, another card will be generated for card two
                cardTwo = (int)((Math.random() * 52) + 1);
            }
            while (cardThree == cardOne || cardThree == cardTwo) {
                //if card three is equal to card one, or card three is equal to card two, another card will be generated for card three
                cardThree = (int)((Math.random() * 52) + 1);
            }
            while (cardFour == cardOne || cardFour == cardThree || cardFour == cardTwo) {
                //if card four is equal to card one, card three, or card two
                //another card will be generated for card four
                cardFour = (int)((Math.random() * 52) + 1);
            }
            while (cardFive == cardOne || cardFive == cardFour || cardFive == cardThree || cardFive == cardTwo) {
                //if card five is equal to card one, card four, card three, or card four,
                //another card will be generated for card five
                cardFive = (int)((Math.random() * 52) + 1);
            }  
            //the next set of if statements are to find the actual value of the card, when generated from a deck

                //start of if statement that finds the 13 mod of card one, if the mod is equal to 0
                //then card one is equal to integer 13
            if (cardOne % 13 == 0) {
                cardOne = (int) 13;

            } else {
                //if the 13 mod of card one is not 0, then the number can be divisible by 13
                //dividing card one by 13 since there are 13 possible value for the cards that can be generated
                cardOne /= 13;
                //if the value of card one divided by 13 is less that 0, then multiply that by 13
                if (cardOne < 0) {
                    cardOne *= 13;
                 //if that value of card one multiplied by 13 is in between 1 and 2...
                } else if (cardOne > 1 && cardOne < 2) {
                //subtract that value of card one by 1, then multiply by 13 to get the actual card value of that card
                    cardOne = cardOne - 1;
                    cardOne *= (int) 13;
                //if that value of card one multiplied by 13 is in between 2 and 3...
                } else if (cardOne > 2 && cardOne < 3) {
                //subtract that value of card one by 2, then multiply by 13 to get the actual card value of that card
                    cardOne = cardOne - 2;
                    cardOne *= (int) 13;
                //if that value of card one multiplied by 13 is in between 3 and 4...
                } else if (cardOne > 3 && cardOne < 4) {
                //subtract that value of card one by 3, then multiply by 13 to get the actual card value of that card
                    cardOne = cardOne - 3;
                    cardOne *= (int) 13;

                }
            }
             //start of if statement that finds the 13 mod of card two, if the mod is equal to 0
                 //then card two is equal to integer 13
            if (cardTwo % 13 == 0) {
                cardTwo = (int) 13;

            } else {
                //if the 13 mod of card one is not 0, then the number can be divisible by 13
                //dividing card two by 13 since there are 13 possible value for the cards that can be generated
                cardTwo /= 13;
                //if the value of card two divided by 13 is less that 0, then multiply that by 13
                if (cardTwo < 0) {
                    cardTwo *= 13;
                //if that value of card two multiplied by 13 is in between 1 and 2...
                } else if (cardTwo > 1 && cardTwo < 2) {
                 //subtract that value of card two by 1, then multiply by 13 to get the actual card value of that card
                    cardTwo = cardTwo - 1;
                    cardTwo *= (int) 13;
                //if that value of card two multiplied by 13 is in between 2 and 3...
                } else if (cardTwo > 2 && cardTwo < 3) {
                 //subtract that value of card two by 2, then multiply by 13 to get the actual card value of that card
                    cardTwo = cardTwo - 2;
                    cardTwo *= (int) 13;
                //if that value of card two multiplied by 13 is in between 3 and 4...
                } else if (cardTwo > 3 && cardTwo < 4) {
                //subtract that value of card two by 3, then multiply by 13 to get the actual card value of that card
                    cardTwo = cardTwo - 3;
                    cardTwo *= (int) 13;

                }
            }
             //start of if statement that finds the 13 mod of card three, if the mod is equal to 0
                //then card three is equal to integer 13
            if (cardThree % 13 == 0) {
                cardThree = (int) 13;

            } else {
                cardThree /= 13;
                //if the value of card two divided by 13 is less that 0, then multiply that by 13
                if (cardThree < 0) {
                    cardThree *= 13;
                //if that value of card three multiplied by 13 is in between 1 and 2...
                } else if (cardThree > 1 && cardThree < 2) {
                //subtract that value of card three by 1, then multiply by 13 to get the actual card value of that card
                    cardThree = cardThree - 1;
                    cardThree *= (int) 13;
                //if that value of card three multiplied by 13 is in between 2 and 3...
                } else if (cardThree > 2 && cardThree < 3) {
                //subtract that value of card three by 2, then multiply by 13 to get the actual card value of that card
                    cardThree = cardThree - 2;
                    cardThree *= (int) 13;
                 //if that value of card three multiplied by 13 is in between 3 and 4...
                } else if (cardThree > 3 && cardThree < 4) {
                //subtract that value of card three by 3, then multiply by 13 to get the actual card value of that card
                    cardThree = cardThree - 3;
                    cardThree *= (int) 13;

                }
            } //start of if statement that finds the 13 mod of card four, if the mod is equal to 0
                 //then card four is equal to integer 13
            if (cardFour % 13 == 0) {
                cardFour = (int) 13;
            } else {
                cardFour /= 13;
                //if the value of card four divided by 13 is less that 0, then multiply that by 13
                if (cardFour < 0) {
                    cardFour *= 13;
                //if that value of card four multiplied by 13 is in between 1 and 2...
                } else if (cardFour > 1 && cardThree < 2) {
                 //subtract that value of card four by 1, then multiply by 13 to get the actual card value of that card
                    cardThree = cardThree - 1;
                    cardThree *= (int) 13;
                //if that value of card four multiplied by 13 is in between 2 and 3...
                } else if (cardThree > 2 && cardThree < 3) {
                //subtract that value of card four by 2, then multiply by 13 to get the actual card value of that card
                    cardThree = cardThree - 2;
                    cardThree *= (int) 13;
                 //if that value of card four multiplied by 13 is in between 3 and 4...
                } else if (cardThree > 3 && cardThree < 4) {
                //subtract that value of card four by 3, then multiply by 13 to get the actual card value of that card
                    cardThree = cardThree - 3;
                    cardThree *= (int) 13;
                }
            }
            //Since we have the actual value , we have 5 cards and each needs to be compared

                //if statement to determine if card one is equal to card two
                //if it is, then it is one of kind, and we increment to save that value
            if (cardOne == cardTwo) {
                oneofKind++;
                //else if statement to determine if card one is equal to card two, and if card three is equal to card four
                //if it is, then it is two of kind, and we increment to save that value
            } else if (cardOne == cardTwo && cardThree == cardFour) {
                twoofKind++;
                //else if statement to determine if card one is equal to card two, and if card two is equal to card three
                //if it is, then it is three of kind, and we increment to save that value
            } else if (cardOne == cardTwo && cardTwo == cardThree) {
                threeofKind++;
                // elseif statement to determine if card one is equal to card two, and if card two is equal to card three and card four
                //if it is, then it is four of kind, and we increment to save that value
            } else if (cardOne == cardTwo && cardTwo == cardThree && cardTwo == cardFour) {
                fourofKind++;
            }

        }
            //declaring the probabilities of each kind of outcome for the game
            //this is done by divivng the actual outcome by the amount of generated handa the user input
            double probFourofKind = ((double) fourofKind / (double) genrHands);
            double probThreeofKind = ((double) threeofKind / (double) genrHands);
            double probTwoofKind = ((double) twoofKind / (double) genrHands);
            double probOneofaKind = ((double) oneofKind / (double) genrHands);

            System.out.println("The number of loops is:" + genrHands); //ouputting the number of hands
            //outputting the outcomes of the probabilities for one, two, three, and four of a kind
            System.out.println("The probability of one of a kind for your hands is:" + probOneofaKind);
            System.out.println("The probability of two of a kind for your hands is: " + probTwoofKind);
            System.out.println("The probability of three of a kind for your hands is: " + probThreeofKind);
            System.out.println("The probability of four of kind for your hands is: " + probFourofKind);
   
     } //end of main class
    }