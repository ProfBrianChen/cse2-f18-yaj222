//////////
/// CSE 02 
/// Yamelin Jaquez
///September 17, 2018
///Finding out the acres of land affected by hurricane percipitation and average rainfall


import java.util.Scanner;
  
  public class Convert{
  
     public static void main(String args []){
      //start of main method for java
     Scanner myScanner = new Scanner(System.in);
       //declaring the instance of Scanner 
       
     System.out.print("Enter number of acres: " );
       //outputting the number of acres so that user can input
       double numAcres = myScanner.nextDouble(); 
       //declaring the number of acres of land and then the method
       
     System.out.print("Enter number of rainfall: ");
       //outputting the number of rainfall so that user can input
       double numRain = myScanner.nextDouble(); 
       //declaring the number of rainfall and then the method
       
       double cubicMiles = numAcres * numRain * 27154; 
       //declaring equation to find how much rain fell in the acres in gallons 
       
       cubicMiles = cubicMiles * .00000000000090817; //converting cubic miles in one gallon which is 9.08 *10^-13
       
       System.out.println(cubicMiles + " cubic miles"); //outputting the cubic miles 

   
                         
     } //end of main method for java
  } //end of class
                      
  