//////////
/// CSE 02 
/// Yamelin Jaquez
///September 17, 2018
//Writing a program that prompts the volume of pyramid

  import java.util.Scanner;
  
    public class Pyramid{
  
     public static void main(String args []){
      //Start of main method for java
       
      Scanner myScanner = new Scanner(System.in);
       //Declaring the instance of Scanner 
       
      System.out.print("Enter the base length: "); 
       //Outputting for the user to enter base length of pyramid
         double  baseLength = myScanner.nextDouble(); 
         //declaring the base length of the pyramid
       
      System.out.print("Enter the base width: "); 
       //Outputting for the user to enter base width length of pyramid 
         double baseWidth = myScanner.nextDouble();
          //declaring the base width of the pyramid
      
         double areaofBase; //declaring area of base
         areaofBase = baseLength * baseWidth; //calculating the area of the base 
       
      System.out.print("Enter the height: "); 
       //outputting for the user to enter the height of the pyramid
          double pyraHeight= myScanner.nextDouble(); 
          //declaring the pyramid's height 
       
          double volumeofPyramid; //declaring the volume of the pyramid
       
          volumeofPyramid =  (areaofBase * pyraHeight) /3;
          //calculating the volume of a pyramid using the formula: V= (l*W*H/3)
         
      System.out.println("The volume of the pyramid is " + volumeofPyramid);
       //outputting the volume of the pyramid
  
         
       } //end of main method for java
    }  //end of class
   
       
       